该项目使用原生的java语言，运用OOAD的思想对ATM的取款用例进行了简单的分析以及具体的实现。

代码不足或者说暂时未想到的地方：对于session与transaction的管理有点不理解，session使用了简单的线程操作，当用户进行插卡操作就开启一个会话，而每一个交易便是一个Transaction，当会话中没有transaction，等待30秒（默认，可以设置session的生命周期）会自动关闭，并引发吞卡操作！


类图：![输入图片说明](https://git.oschina.net/uploads/images/2017/0513/115601_4c4ade15_942742.jpeg "在这里输入图片标题")
顺序图：![输入图片说明](https://git.oschina.net/uploads/images/2017/0513/115630_f3c4dd18_942742.jpeg "在这里输入图片标题")

顺序图并不完善，对于ifelse的条件没有进行下一步划分，读者可以自行完善。

程序测试入口：

控制台页面可运行：test/com/scau/mis35/service/Test.java

swing界面运行：src/com/scau/mis35/view/UI.java（swing界面不完善，可参照Test.java自行扩展）





