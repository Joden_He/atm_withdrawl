package com.scau.mis35.service;

import java.util.Map;
import java.util.Scanner;

import com.scau.mis35.util.RemotedBank;

public class Test {
	private static int count = 0;//用于标记输入密码次数

	public static void main(String[] args) {
		//初始化ＡＴＭ
		ATM atm = new ATM(123, "中国农业银行", new RemotedBank());
		Scanner sc = new Scanner(System.in);
		plginCard(sc,atm);
	}

	public static void plginCard(Scanner sc,ATM atm){
		count = 0;
		System.out.println("======================================");
		System.out.println("系统的静态数据："+atm.getRemotedBank());
		System.out.println("======================================");
		System.out.println("模拟插卡（请输入卡号）：");
		Card card = atm.pluginCard(sc.nextLine());
		f(card,atm,sc);
	}

	public static void f(Card card,ATM atm,Scanner sc){
		System.out.println("请选择操作：");
		System.out.println("1 输入密码，2 退卡（输入相应数字）：");
		switch (Integer.parseInt(sc.nextLine())) {
		case 1:
			validateAccount(card,atm,sc);
			break;
		case 2:
			atm.ejectCard();
			plginCard(sc,atm);
			break;

		default:
			break;
		}
	}

	public static void validateAccount(Card card,ATM atm,Scanner sc){
		System.out.println("请输入密码：");
		if (atm.validateAccount(card,sc.nextLine())) {
			makeTransaction(atm, sc);
		}else{
			System.out.println("密码错误！");
			count+=1;
			if (count>=3) {
				atm.retainCard();
			}else{
				f(card,atm,sc);
			}
			
		}
	}

	public static void makeTransaction(ATM atm,Scanner sc){
		System.out.println("请选择交易类型：");
		System.out.println("1取款，2存款，3转账，4查询,5退卡(输入相应的数字):");
		switch (Integer.parseInt(sc.nextLine())) {
		case Transcation.WITH_DRAWAL:
			System.out.println("请输入取款金额（100的倍数）：");
			int amount = Integer.parseInt(sc.nextLine());
			if(amount%100==0){
				Map<String,Object> result = atm.withdrawal(amount);
				if ((boolean)result.get("status") ) {
					System.out.println(result.get("msg"));
					System.out.println("是否打印凭条（是输入１，否输入０）：");
					if (1==Integer.parseInt(sc.nextLine())) {
						atm.printRecipt();
					}
					makeTransaction(atm, sc);
				}
				else{
					System.out.println(result.get("msg"));
					makeTransaction(atm, sc);
				}
			}else{
				System.out.println("输入金额必须为100的倍数：");
				makeTransaction(atm, sc);
			}
			break;
		case 5:
			atm.ejectCard();
			plginCard(sc,atm);
			break;

		default:
			System.out.println("暂时不支持该功能");
			makeTransaction(atm, sc);
			break;
		}
	}
}
