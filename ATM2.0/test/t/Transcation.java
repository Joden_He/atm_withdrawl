package t;

public class Transcation {
	static final int WITH_DRAWAL = 1;//取款
	static final int DEPOSIT = 2;//存款
	static final int TRANSFER = 3;//转账
	static final int INQUIRY = 4;//查询
	//TODO 可扩展其他事务
	private String description;
	private int amount;//交易处理的金额

	public Transcation(int pin,int amount){
		switch (pin) {
		case WITH_DRAWAL:
			description = "取款";
			this.amount = amount;
			break;
		case DEPOSIT:
			description = "存款款";
			this.amount = amount;
			break;
		case TRANSFER:
			description = "转账";
			this.amount = amount;
			break;
		case INQUIRY:
			description = "查询";
			this.amount = amount;
			break;
		default:
			this.amount = amount;
			description = "其他";
			break;
		}
	}

	public String getDescription() {
		return description;
	}

	public int getAmount() {
		return amount;
	}
	
}
