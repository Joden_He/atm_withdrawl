package t;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Receipt {
	private Date time =new Date();
	private  String headingPortion;//头部
	private StringBuilder mainPortion = new StringBuilder();//主体部分

	public String print(ATM atm, Card card, Transcation transcation){
		
		SimpleDateFormat dateformat=new SimpleDateFormat("yyyy/MM/dd");
		SimpleDateFormat dateformat2=new SimpleDateFormat("HH:mm:ss");
		headingPortion = atm.getBankName();
		//初始化静态内容
		mainPortion.append("自动柜员机客户凭单\n日期\t\t时间\t\tATM编号\n");
		mainPortion.append(dateformat.format(time)+"\t"+dateformat2.format(time)+"\t"+atm.getId()+"\n");
		mainPortion.append("交易金额："+transcation.getAmount()+"\t\t手续费：\n");
		mainPortion.append("转入帐号：："+card.getCardNo()+"\n");
		mainPortion.append("取款\t\t存款\t\t转账\n");
		switch (transcation.getDescription()) {
		case "取款":
			mainPortion.append("***\n");
			break;
		case "存款":
			mainPortion.append("\t\t***\n");
			break;
		case "转账":
			mainPortion.append("\t\t\t\t***\n");
			break;
		default:
			break;
		}
		return headingPortion+"\n"+mainPortion;
	}

	public static void main(String[] args) {
		
		ATM atm = new ATM(123, "中国农业银行", new RemotedBank());
		Receipt r = new Receipt();
		String a = r.print(atm, new Card("44088131312121"), new Transcation(1,2000));
		System.out.println(a);
	}

}
