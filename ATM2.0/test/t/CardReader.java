package t;

public class CardReader {

	//读卡
	public Card readCard(String cardNo){
		return new Card(cardNo);
	}

	//退卡
	public void ejectCard(){
		System.out.println("退卡！");
	}

	//吞卡
	public void retainCard(){
		System.out.println("吞卡！");
	}
}
