package com.scau.mis35.service;

public class Card {
	private String cardNo;//卡号

	public Card(String cardNo) {
		this.cardNo = cardNo;
	}


	public String getCardNo() {
		return cardNo;
	}


	@Override
	public String toString() {
		return "Card [cardNo=" + cardNo + "]";
	}
}
