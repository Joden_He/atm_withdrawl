package com.scau.mis35.service;

import java.util.Date;
import java.util.UUID;

public class Session extends Thread{
	private final String SESSION_ID = UUID.randomUUID().toString();//session唯一标记
	private long lifecycle = 30000;//生命周期，默认30毫秒
	private volatile Transcation transcation;//进行的交易
	public volatile boolean closeState = false;

	/**
	 * 构造方法
	 */
	public Session() {
		super();
	}
	
	/**
	 * 构造方法
	 * @param lifecycle 生命周期，毫秒数
	 */
	public Session( long lifecycle) {
		super();
		this.lifecycle = lifecycle;
	}

	@Override
	public void run() {
		Date now = new Date();
		while(!closeState||null!=transcation){
			if (null == transcation && new Date().getTime() - now.getTime() >=lifecycle) {
				break;
			}else if(null != transcation){
				now = new Date();
			}
		}
		
		System.out.println("session结束，已自动退卡");
	}


	/**
	 * 添加事务
	 * @param transcation
	 */
	public void setTranscation(Transcation transcation) {
		this.transcation = transcation;
	}

	public String getSESSION_ID() {
		return SESSION_ID;
	}

	public long getLifecycle() {
		return lifecycle;
	}

	public Transcation getTranscation() {
		return transcation;
	}
	
}
