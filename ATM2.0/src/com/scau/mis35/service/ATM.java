package com.scau.mis35.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.scau.mis35.phys.CardReader;
import com.scau.mis35.phys.CashDispenser;
import com.scau.mis35.phys.Printer;
import com.scau.mis35.util.Log;
import com.scau.mis35.util.RemotedBank;

public class ATM {
	private int id;//每个ATM应该拥有唯一的id编号
	private String bankName;//ATM机所属的银行

	private Session session;
	private Card card;
	private CardReader cardReader = new CardReader();//读卡器
	private CashDispenser cashDispenser = new CashDispenser();//出钞器
	private Printer printer = new Printer();//出钞器
	private Log log = new Log();//日志
	private RemotedBank remotedBank;//远程的银行系统

	//初始化ATM
	public ATM(int id,String bankName,RemotedBank remotedBank) {
		this.id = id;
		this.bankName = bankName;
		this.remotedBank = remotedBank;
		log.addMessage(new Date()+"：初始化ATM");
	}

	//插卡
	public Card pluginCard(String cardNo){
		log.addMessage(new Date()+"：用户进行插卡操作");
		this.card = cardReader.readCard(cardNo);
		session = new Session();
		session.start();//开启session
		log.addMessage(new Date()+"：开启一次新会话,sessionId:"+session.getId());
		return card;
	}

	//输入密码并进行密码验证
	public boolean validateAccount(Card card,String password){
		log.addMessage(new Date()+"：密码校验："+card);
		if (remotedBank.validateAccount(card.getCardNo(),password)) {
			log.addMessage(new Date()+"：密码校验成功");
			return true;
		}
		log.addMessage(new Date()+"：密码校验失败");
		return false;
	}

	//取款
	public Map<String,Object> withdrawal(int amount){
		Map<String,Object> result = new HashMap<String,Object>();
		//开启事务
		session.setTranscation(new Transcation(Transcation.WITH_DRAWAL,amount));
		log.addMessage(new Date()+"：进行取款事务操作");
		if (remotedBank.validateBalance(card.getCardNo(),amount)) {//远程判断账户余额是否足够
			//出钞
			result = cashDispenser.spiltCash(amount);
			log.addMessage(new Date()+"："+result.get("msg")+"取款金额为："+amount);
			return result;
		}
		//关闭事务
		session.setTranscation(null);
		result .put("status", false);
		result .put("msg", "账户余额不足，操作失败");
		log.addMessage(new Date()+"：账户余额不足，操作失败");
		return result;

	}

	//退卡
	public void ejectCard(){
		cardReader.ejectCard();
		//关闭事务
		session.setTranscation(null);
		session.closeState = true;
		log.addMessage(new Date()+"：关闭事务，退卡");
	}

	//退卡
	public void retainCard(){
		cardReader.retainCard();
		//关闭事务
		session.setTranscation(null);
		session.closeState = true;
		log.addMessage(new Date()+"：关闭事务，吞卡");
	}

	//打印凭条
	public void printRecipt(){
		log.addMessage(new Date()+"：打印凭条");
		Receipt receipt = new Receipt(this, card, session.getTranscation());
		printer.print(receipt);
	}

	public int getId() {
		return id;
	}

	public String getBankName() {
		return bankName;
	}

	public Session getSession() {
		return session;
	}

	public Card getCard() {
		return card;
	}

	public CardReader getCardReader() {
		return cardReader;
	}

	public CashDispenser getCashDispenser() {
		return cashDispenser;
	}

	public Log getLog() {
		return log;
	}

	public RemotedBank getRemotedBank() {
		return remotedBank;
	}

	@Override
	public String toString() {
		return "ATM [id=" + id + ", bankName=" + bankName + ", session=" + session + ", card=" + card + ", cardReader="
				+ cardReader + ", cashDispenser=" + cashDispenser + ", log=" + log + ", remotedBank=" + remotedBank
				+ "]";
	}
}
