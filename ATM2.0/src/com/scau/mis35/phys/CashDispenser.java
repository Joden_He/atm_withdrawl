package com.scau.mis35.phys;

import java.util.HashMap;
import java.util.Map;

public class CashDispenser {
	private int cashOnHand;//拥有的现金量

	public CashDispenser() {
		cashOnHand = 100000000;//初始化钱
	}
	
	//加钱
	public void addCash(int amount) {
		this.cashOnHand += amount;
	}
	
	public Map<String,Object> spiltCash(int amount) {
		Map<String,Object> result = new HashMap<String,Object>();
		if (amount > cashOnHand) {
			result.put("status", false);
			result.put("msg", "ATM没有足够的现金！");
			return result;
		}
		this.cashOnHand -= amount;
		result.put("status", true);
		result.put("msg", "交易成功！");
		return result;
	}

	public int getCashOnHand() {
		return cashOnHand;
	}

}
