package com.scau.mis35.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class Log {
	private List<String> message = new ArrayList<String>();//记录日志

	public void addMessage(String m) {
		this.message.add(m);
		this.WriteStringToFile("logs/log.log",m+"\n");
	}
	
	public List<String> getMessage() {
		return message;
	}
	
	public void WriteStringToFile(String filePath,String m) {  
        try { 
        	File file = new File(filePath);
        	if (!file.exists()) {
        		file.createNewFile();
			}
            FileWriter fw = new FileWriter(filePath, true); 
            BufferedWriter bw = new BufferedWriter(fw);  
            bw.append(m);  
            bw.close();  
            fw.close();  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
    }  

	@Override
	public String toString() {
		return "Log [message=" + message + "]";
	}
	
}
