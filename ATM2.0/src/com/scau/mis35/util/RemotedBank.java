package com.scau.mis35.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RemotedBank {

	List <Map<String,Object>> accounts=new ArrayList<Map<String,Object>>();

	//初始化
	public RemotedBank() {
		for(int i=0 ;i<10;i++){
			Map<String,Object> account = new HashMap<String,Object>();
			account.put("account", "123"+i);
			account.put("password", "123456");
			account.put("balance", 20400.33);
			accounts.add(account);
		}
	}

	//验证余额是否足够
	public boolean validateBalance(String cardNO, Integer amount) {
		for (Map<String, Object> account : accounts) {
			if(account.get("account").equals(cardNO)&&((Double)account.get("balance"))-amount >= 0){
				account.put("balance", (Double)account.get("balance")-amount);
				return true;
			}
		}
		return false;
	}

	//验证账户
	public boolean validateAccount(String cardNO, String password) {
		for (Map<String, Object> account : accounts) {
			if(account.get("account").equals(cardNO)&&account.get("password").equals(password)){
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		String str = "远程银行系统的账户信息：\n";
		for (Map<String, Object> map : accounts) {
			str+=map+"\n";
		}
		return str;
	}

}
