package com.scau.mis35.view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.scau.mis35.service.ATM;
import com.scau.mis35.service.Card;
import com.scau.mis35.util.RemotedBank;

public class UI extends JFrame implements ActionListener{

	private static final long serialVersionUID = 1L;

	private ATM atm;
	private Card card;
	private CardLayout cardLayout,cardLayout2;
	private JPanel pCenter,pCenter2;
	private JTextField ATMNO,bankName,cardNO,password,withdrawalText;
	private JButton initPanelButton,pluginCard,validatePassowrd,returnBtn,witdrawalBtn,printBtn;

	public UI(){
		cardLayout=new CardLayout();
		Container  con=getContentPane();
		pCenter=new JPanel();
		pCenter.setLayout(cardLayout);

		pCenter.add("初始化界面",getInitPanel());
		pCenter.add("插卡",getPluginCardPanel());
		pCenter.add("ATM界面",getATMPanel());
		con.add(pCenter,BorderLayout.CENTER);
		con.validate();

		setTitle("35组ATM取款模");
		setBounds(100, 100, 450, 300);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==initPanelButton) {
			//初始化ＡＴＭ
			this.atm = new ATM(Integer.parseInt(ATMNO.getText()), bankName.getText(), new RemotedBank());
			//显示ＡＴＭ界面
			cardLayout.show(pCenter,"插卡");
		}
		if (e.getSource()==pluginCard) {
			this.card = atm.pluginCard(cardNO.getText());
			cardLayout.show(pCenter,"ATM界面");
		}
		if (e.getSource()==validatePassowrd) {
			if (atm.validateAccount(card,password.getText())) {
				cardLayout2.show(pCenter2,"取款界面");
			}else{
				JOptionPane.showMessageDialog(null, "密码错误", "ERROR", JOptionPane.ERROR_MESSAGE);
				cardLayout2.show(pCenter2,"输入密码");
			}
		}
		if (e.getSource()==witdrawalBtn) {
			Map<String,Object> result = atm.withdrawal(Integer.parseInt(withdrawalText.getText()));
			if ((boolean) result.get("status")) {
				JOptionPane.showMessageDialog(null, result.get("msg"), "SUCCESS", JOptionPane.OK_OPTION);
				cardLayout2.show(pCenter2,"打印凭条");
			}
			else{
				JOptionPane.showMessageDialog(null, result.get("msg"), "ERROR", JOptionPane.ERROR_MESSAGE);
				cardLayout2.show(pCenter2,"取款界面");
			}
		}
		if (e.getSource()==printBtn) {
			atm.printRecipt();
			//JOptionPane.showMessageDialog(null, atm.printRecipt(), "凭条内容", JOptionPane.OK_OPTION);
			cardLayout.show(pCenter,"ATM界面");
		}
	}

	//初始化数据页面
	private JPanel getInitPanel(){
		JPanel panel = new JPanel();
		JPanel p = new JPanel();
		JPanel p_1 = new JPanel();
		JPanel p_2 = new JPanel();
		ATMNO = new JTextField();
		bankName = new JTextField();
		initPanelButton = new JButton("确定");
		initPanelButton.addActionListener(this);

		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
		p_1.setLayout(new BoxLayout(p_1, BoxLayout.X_AXIS));
		p_2.setLayout(new BoxLayout(p_2, BoxLayout.X_AXIS));
		p.add(Box.createVerticalStrut(10));
		p.add(p_1);
		p.add(Box.createVerticalStrut(10));
		p.add(p_2);
		p.add(Box.createVerticalStrut(50));
		p.add(initPanelButton, "确定");
		p.add(Box.createVerticalStrut(40));

		p_1.add(Box.createHorizontalStrut(10));
		p_1.add(new JLabel("ＡＴＭ编号："));
		p_1.add(Box.createHorizontalStrut(10));
		p_1.add(ATMNO,"ATM编号");
		p_1.add(Box.createHorizontalStrut(50));

		p_2.add(Box.createHorizontalStrut(10));
		p_2.add(new JLabel("银行名称：　"));
		p_2.add(Box.createHorizontalStrut(10));
		p_2.add(bankName,"银行名称");
		p_2.add(Box.createHorizontalStrut(50));

		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(Box.createVerticalStrut(10));
		JLabel label = new JLabel("初始化数据");
		label.setAlignmentX(0.5f);
		panel.add(label, JLabel.CENTER_ALIGNMENT);
		panel.add(Box.createVerticalStrut(10));
		panel.add(p);
		panel.add(Box.createVerticalStrut(100));

		return panel;
	}

	//ＡＴＭ界面
	private JPanel getATMPanel(){
		cardLayout2=new CardLayout();
		Container  con=getContentPane();
		pCenter2 = new JPanel();
		pCenter2.setLayout(cardLayout2);

		//输入密码
		JPanel p1 = new JPanel();
		p1.setLayout(new BoxLayout(p1, BoxLayout.Y_AXIS));
		validatePassowrd = new JButton("确定");//验证密码
		validatePassowrd.addActionListener(this);
		password = new JTextField();
		JPanel p1_1 = new JPanel();
		p1_1.setLayout(new BoxLayout(p1_1, BoxLayout.X_AXIS));
		p1_1.add(new JLabel("密码：）"));
		p1_1.add(password);

		p1.add(Box.createVerticalStrut(100));
		p1.add(p1_1);
		p1.add(Box.createVerticalStrut(10));
		p1.add(validatePassowrd);
		p1.add(Box.createVerticalStrut(170));

		//密码错误界面
		JPanel p2 = new JPanel();//输入密码
		returnBtn = new JButton("返回");
		p2.add(new JLabel("密码错误"));
		p2.add(returnBtn);

		//取款界面
		JPanel p3 = new JPanel();
		p3.setLayout(new BoxLayout(p3, BoxLayout.Y_AXIS));
		withdrawalText = new JTextField(); 
		witdrawalBtn = new JButton("确定");
		witdrawalBtn.addActionListener(this);
		JPanel p3_1 = new JPanel();
		p3_1.setLayout(new BoxLayout(p3_1, BoxLayout.X_AXIS));
		p3_1.add(new JLabel("输入取款金额："));
		p3_1.add(withdrawalText);
		
		p3.add(Box.createVerticalStrut(100));
		p3.add(p3_1);
		p3.add(Box.createVerticalStrut(10));
		p3.add(witdrawalBtn);
		p3.add(Box.createVerticalStrut(170));
		
		//打印凭条
		JPanel p4 = new JPanel();
		printBtn = new JButton("打印凭条");
		printBtn.addActionListener(this);
		p4.add(printBtn);
		
		
		pCenter2.add("输入密码",p1);
		pCenter2.add("密码错误",p2);
		pCenter2.add("取款界面",p3);
		pCenter2.add("打印凭条",p4);
		con.add(pCenter2,BorderLayout.CENTER);
		con.validate();


		return pCenter2;
	}

	//插卡界面
	private JPanel getPluginCardPanel(){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		pluginCard = new JButton("确定");
		pluginCard.addActionListener(this);
		cardNO = new JTextField();
		JPanel p2 = new JPanel();
		p2.setLayout(new BoxLayout(p2, BoxLayout.X_AXIS));
		p2.add(new JLabel("模拟插卡（输入卡号即可：）"));
		p2.add(cardNO);

		panel.add(Box.createVerticalStrut(100));
		panel.add(p2);
		panel.add(Box.createVerticalStrut(10));
		panel.add(pluginCard);
		panel.add(Box.createVerticalStrut(170));
		return panel;
	}

	public static void main(String[] args) {
		new UI();
	}

}
